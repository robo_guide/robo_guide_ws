#!/usr/bin/env bash

# multi machine setup
export ROS_MASTER_URI=http://172.31.1.149:11311 # laptop as master
export ROS_IP=172.31.1.148 # jetson static ip
# setup display for GL support
export DISPLAY=:0

# source the workspace
source /home/nvidia/code/robo_guide_ws/devel/setup.bash

exec "$@"
