# ROS Workspace
This is the ROS workspace which contains all of the projects needed for the
robo_guide object tracking.

Some repositories use the **git-lfs** extension which has to be installed to pull
all neccessary files.

The repository uses submodules to pull in all of the projects so this command
for cloning:

```bash
git clone --recursive git@git.rwth-aachen.de:robo_guide/robo_guide_ws.git
```

# Dependencies
Most of dependencies are included in the full ROS-Desktop environment.
The other dependencies are either included as submodules in this workspace or 
can be installed via rosdep. 

Only **visp** might require manual installation via an apt command (see below).

## ROS
ROS Perception (compare with this [REP](http://www.ros.org/reps/rep-0142.html))
includes most of the packages that are needed. Otherwise install them via apt
for your ros distro.
In detail these packages are required:
- [ROS-Desktop](http://wiki.ros.org/melodic) Install (for catkin, messages, 
nodes, ...)
- [tf2_geometry_msgs](http://wiki.ros.org/tf2_geometry_msgs) for conversion of 
tf2 messages. Install via apt.
- [realsense2_camera](https://github.com/intel-ros/realsense) as data provider 
for the depth images. With a bit of tweaking of the launch files, the tracker
should also run with OpenNI devices. Install as stated in realsense README.
Make sure you have the
[ddynamic_reconfigure](https://github.com/pal-robotics/ddynamic_reconfigure)
package installed.
- [image_pipeline](http://wiki.ros.org/image_pipeline?distro=melodic) for
preprocessing of the raw camera data
- [visp_hand2eye_calibration](http://wiki.ros.org/visp_hand2eye_calibration) for
the [easy_handeye](https://github.com/IFL-CAMP/easy_handeye) calibration.

If you have ROS melodic installed use rosdep inside the workspace folder:
```bash
rosdep install --from-paths src --ignore-src -r -y
```

Unfortunately the realsense camera needs some manual installation steps.
But you might remove the package from the workspace and use your camera
driver instead. The same goes for the iiwa and hand-eye-calibration
related packages which are only required when mounting the camera on a
robot.

## [visp](https://visp.inria.fr/install/)
Needed for handy-eye-calibration install via:
```bash
sudo apt install libvisp-dev libvisp-doc visp-images-data
```

## Rendering
The dependencies of scigl_render are handled via rosdep.

## Unit testing
Dependencies can be installed via rosdep:
* GTest

Note that GTest only installs the sources under Ubuntu, execute:
```sh
sudo apt install libgtest-dev build-essential cmake
cd /usr/src/googletest
sudo cmake .
sudo cmake --build . --target install
```

## Robot communication
[protocol buffers](https://github.com/protocolbuffers/protobuf/) are installed via rosdep.

# Build
In the past Ubuntu 16.04 and ROS Kinetic were used, which should still work.
Right now Ubuntu 18.04 an ROS Melodic are in use and tested.

Since this is a mixed catkin & cmake workspace, the easiest and fastest way is
to use the [catkin tools](https://catkin-tools.readthedocs.io/en/latest/),
[catkin_make_isolated](http://www.ros.org/reps/rep-0134.html) or
[colcon build](https://colcon.readthedocs.io/en/latest/user/quick-start.html).

It is required to set a profile for compilation. Use the catkin_configure.bash
script before you use catkin build. The srcipt calls:
```bash
catkin config --cmake-args -DCMAKE_BUILD_TYPE=Release
```
# Jetson config
IP configuration:

- iiwa: 172.31.1.147
- jetson: 172.31.1.148
- laptop: 172.31.1.149

To enable comfortable remote access setup ssh keys on the laptop and copy them to the jetson.
It is important to add the jetson to the known hosts:

```bash
ssh-keygen -t rsa -b 4096 -C "email@example.com"
ssh-copy-id -i .ssh/id_rsa.pub 
ssh -oHostKeyAlgorithms='ssh-rsa' nvidia@172.31.1.148
```

Export the environment variables on te laptop (e.g. in .bashrc):
```bash
export ROS_MASTER_URI=http://172.31.1.148:11311 # jetson as master
export ROS_IP=172.31.1.149
```

And on the jetson:
```bash
export ROS_MASTER_URI=http://172.31.1.148:11311 # jetson as master
export ROS_IP=172.31.1.8
```

Optionally add them to /etc/hosts for easier ssh commands:
```bash
172.31.1.147 iiwa
172.31.1.148 jetson
```
